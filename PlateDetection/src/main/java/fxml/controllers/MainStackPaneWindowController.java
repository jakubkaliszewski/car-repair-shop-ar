package fxml.controllers;

import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelFormat;
import javafx.scene.image.WritablePixelFormat;
import javafx.stage.FileChooser;
import maskRCNN.GraphBuilder;
import maskRCNN.MaskRCNNModel;
import maskRCNN.TensorUtils;
import org.tensorflow.*;
import org.tensorflow.op.image.DecodeJpeg;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.FloatBuffer;
import java.util.Arrays;

public class MainStackPaneWindowController {

    @FXML
    public Label loadedImageLabel;
    @FXML
    public Button loadImageButton;
    @FXML
    public ImageView loadedImageView;
    @FXML
    public ImageView resultImageView;
    @FXML
    public Label resultLabel;
    @FXML
    public Label scoreLabel;
    @FXML
    public Label resultImageLabel;
    @FXML
    public Button analyzeImageButton;

    private FileChooser fileChooser;

   // private final MaskRCNNModel model;

    public MainStackPaneWindowController() throws URISyntaxException, IOException {
        //model = new MaskRCNNModel();
    }

    public void onLoadImageButtonAction(ActionEvent actionEvent) {
        System.out.println("Wczytanie");
        fileChooser = new FileChooser();
        fileChooser.setTitle("Wybierz plik jpeg");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Pliki jpeg", "*.jpg", "*.jpeg"));
        //uruchomienie okna wyboru pliku
        var file = fileChooser.showOpenDialog(null);
        try {
            Image image = new Image(new FileInputStream(file));
            loadedImageView.setImage(image);
        } catch (FileNotFoundException exception) {
            createAlertWindow(String.format("Nie wczytano poprawnie obrazu %s!", file.getAbsolutePath()), exception.toString());
        }

    }

    public void onAnalyzeImageButtonAction(ActionEvent actionEvent) {
        System.out.println("Analiza");
        Image imageToAnalyze = loadedImageView.getImage();
        BufferedImage bImage = SwingFXUtils.fromFXImage(imageToAnalyze, null);
        byte[] imageBytes;
        try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
            ImageIO.write(bImage, "jpg", stream);
            imageBytes = stream.toByteArray();

        } catch (IOException ioException) {
            createAlertWindow("Nie udało się zanalizować obrazu!", ioException.getMessage());
            return;
        }
        /*
        Próba uruchomienia zamrożonego grafu tensorflow wygenerowanego za pomocą narzędzi - zakończone wyjątkiem niżej
        try (Tensor<Float> image = TensorUtils.constructAndExecuteGraphToNormalizeImage(imageBytes, (int) imageToAnalyze.getHeight(), (int) imageToAnalyze.getWidth())) {
            var inputImageMetaTensor = Tensor.create((float) model.IMAGE_META_SIZE);
            var input_anchorsTensor = Tensor.create(4f);
            try (Session sess = new Session(model.getGraph())) {
                var result = sess.runner().fetch("mrcnn_class/Softmax")
                        .feed("input_image", image)
                        .feed("input_image_meta", inputImageMetaTensor)
                        .feed("input_anchors", input_anchorsTensor)
                        .run().get(0);
                System.out.println(result);
            } catch (Exception e) {//java.lang.IllegalArgumentException: Index out of range using input dim 0; input has only 0 dims [[{{node ROI/strided_slice_6}}]]
                createAlertWindow("Nie udana analiza obrazu!", e.getMessage());
            }

        }

         */
    }

    private void createAlertWindow(String message, String exceptionMessage) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Błąd");
        alert.setHeaderText(message);
        alert.setContentText(exceptionMessage);

        alert.showAndWait();
    }


}
