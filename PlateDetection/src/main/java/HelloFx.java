import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.io.IOException;

public class HelloFx extends Application {

    private final String TITLE = "Test MaskRCNN - tablice rejestracyjne";

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getResource("/fxml/MainStackPaneWindow.fxml"));
        StackPane mainPane = loader.load();

        Scene scene = new Scene(mainPane, mainPane.getMaxHeight(), mainPane.getMaxWidth());
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setTitle(TITLE);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }

}