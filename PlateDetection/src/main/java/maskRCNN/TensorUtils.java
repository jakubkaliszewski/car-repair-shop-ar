package maskRCNN;

import org.tensorflow.Graph;
import org.tensorflow.Output;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

public class TensorUtils {
    public static Tensor<Float> constructAndExecuteGraphToNormalizeImage(byte[] imageBytes, int height, int width) {
        try (Graph g = new Graph()) {
            GraphBuilder b = new GraphBuilder(g);
            // Some constants specific to the pre-trained model at:
            // https://storage.googleapis.com/download.tensorflow.org/models/inception5h.zip
            //
            // - The model was trained with images scaled to 224x224 pixels.
            // - The colors, represented as R, G, B in 1-byte each were converted to
            //   float using (value - Mean)/Scale.
            final float mean = 117f;
            final float scale = 1f;

            // Since the graph is being constructed once per execution here, we can use a constant for the
            // input image. If the graph were to be re-used for multiple input images, a placeholder would
            // have been more appropriate.
            final Output<String> input = b.constant("input", imageBytes);
            final Output<Float> output =
                    b.div(
                            b.sub(
                                    b.resizeBilinear(
                                            b.expandDims(
                                                    b.cast(b.decodeJpeg(input, 3), Float.class),
                                                    b.constant("make_batch", 0)),
                                            b.constant("size", new int[] {height, width})),
                                    b.constant("mean", mean)),
                            b.constant("scale", scale));
            try (Session s = new Session(g)) {
                // Generally, there may be multiple output tensors, all of them must be closed to prevent resource leaks.
                return s.runner().fetch(output.op().name()).run().get(0).expect(Float.class);
            }
        }
    }

    public static Tensor<?> expandDimension(Tensor<?> tensor, int dimension) {
        try (Graph g = new Graph()) {
            final GraphBuilder b = new GraphBuilder(g);

            final Output input = b.constant("input", tensor.bytesValue());
            final Output output = g.opBuilder("ExpandDims", "ExpandDims").addInput(input).addInput(b
                    .constant("dimension", dimension)).build().output(0);

            try (Session s = new Session(g)) {
                Tensor<?> result = (Tensor<?>) s.runner().fetch(output.op().name()).run().get(0);
                return result;
            }
        }
    }
}
