package maskRCNN;

import org.tensorflow.Graph;
import org.tensorflow.Session;
import org.tensorflow.Tensor;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MaskRCNNModel {

    private static final String MODEL_FILENAME = "saved_model.pb";
    private static final int NUM_CLASSES = 2;
    public static final int IMAGE_META_SIZE = 1 + 3 + 3 + 4 + 1 + NUM_CLASSES;
    private static final Map<String, Object> DEFAULT_INPUT_NODES = new HashMap<>() {

        private static final long serialVersionUID = 1L;
        {
            put("input_image", null);
            put("input_image_meta", null);
            put("input_anchors", null);
        }
    };

    private static final List<String> OUTPUT_NODE_NAMES = Arrays.asList(
            "output_detections",
            "output_mrcnn_class",
            "output_mrcnn_bbox",
            "output_mrcnn_mask",
            "output_rois");

    private Tensor<?> imageMetadata;
    private Tensor<?> anchors;

    private final Graph graph;

    public MaskRCNNModel() throws URISyntaxException, IOException {
        var res = getClass().getClassLoader().getResource(MODEL_FILENAME).toURI();
        File f = new File(res);
        var p = f.getParent();

        graph = new Graph();
        graph.importGraphDef(Files.readAllBytes(Path.of(res.getPath())));
        graph.operations().forEachRemaining(c -> System.out.println(c.name()));
    }

    public void run(Tensor<?> moldedImage){
        // Get input nodes as tensor.
        Map<String, Object> inputNodes = new HashMap<>(DEFAULT_INPUT_NODES);

        moldedImage = TensorUtils.expandDimension(moldedImage, 0);
        inputNodes.put("input_image", moldedImage);

        imageMetadata = TensorUtils.expandDimension(imageMetadata, 0);
        inputNodes.put("input_image_meta", imageMetadata);

        anchors = TensorUtils.expandDimension(anchors, 0);
        inputNodes.put("input_anchors", anchors);

        // Setup the runner with input and output nodes.
        var sess = new Session(graph);
        Session.Runner runner = sess.runner();
        for (Map.Entry<String, Object> entry : inputNodes.entrySet()) {
            runner = runner.feed(entry.getKey(), (Tensor<?>) entry.getValue());
        }

        for (String outputName : OUTPUT_NODE_NAMES) {
            runner = runner.fetch(outputName);
        }

        // Run the model
        final List<Tensor<?>> outputsList = runner.run();

        // Save results in a dict
        Tensor<?> detections = outputsList.get(0);
        Tensor<?> mrcnn_class = outputsList.get(1);
        Tensor<?> mrcnn_bbox = outputsList.get(2);
        Tensor<?> mrcnn_mask = outputsList.get(3);
        Tensor<?> rois = outputsList.get(4);

        System.out.println("detections : " + detections);
        System.out.println("mrcnn_class : " + mrcnn_class);
        System.out.println("mrcnn_bbox : " + mrcnn_bbox);
        System.out.println("mrcnn_mask : " + mrcnn_mask);
        System.out.println("rois : " + rois);

        sess.close();
    }

    public void close() {
        graph.close();
    }

    public Graph getGraph() {
        return graph;
    }
}
