import maskRCNN.MaskRCNNModel;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.tensorflow.*;

public class HelloCV {
    public static void main(String[] args) throws Exception{
        nu.pattern.OpenCV.loadShared();
        Mat mat = Mat.eye(3, 3, CvType.CV_8UC1);
        System.out.println("mat = " + mat.dump());
        System.out.println(TensorFlow.version());

        try {
            MaskRCNNModel model = new MaskRCNNModel();
            model.close();
            System.out.println("Model is loaded!");
        }catch (Exception e){
            System.out.println("Model isn't loaded!");
        }


        //var model = new Model();

       // try (Graph g = new Graph()) {
        //    final String value = "Hello from " + TensorFlow.version();

            // Construct the computation graph with a single operation, a constant
            // named "MyConst" with a value "value".

            /*try (var t = Tensor.create(value.getBytes("UTF-8"))) {
                // The Java API doesn't yet include convenience functions for adding operations.
                g.opBuilder("Const", "MyConst").setAttr("dtype", t.dataType()).setAttr("value", t).build();
            }

            // Execute the "MyConst" operation in a Session.
            try (Session s = new Session(g);
                 // Generally, there may be multiple output tensors,
                 // all of them must be closed to prevent resource leaks.
                 Tensor output = s.runner().fetch("MyConst").run().get(0)) {
                System.out.println(new String(output.bytesValue(), "UTF-8"));
            }
            */
       // }
    }
}
