package pl.kaliszewski;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collections;
import java.util.HashSet;

@Path("/car")
@Produces(MediaType.APPLICATION_JSON)
public class ExampleResource {

    HashSet<Car> allCars = new HashSet<>(Collections.singletonList(new Car("Palio Weekend", "Fiat",  2000L)));

    @GET
    public HashSet<Car> car() {
        return allCars;
    }

    @POST
    public Response addCar(Car car){
        allCars.add(car);
        return Response.created(URI.create("Car")).build();
    }
}
