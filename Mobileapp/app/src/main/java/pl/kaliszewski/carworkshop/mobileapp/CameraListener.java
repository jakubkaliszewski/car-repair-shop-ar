package pl.kaliszewski.carworkshop.mobileapp;

import android.util.Log;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.camera.core.ImageCapture;

import java.io.File;

public class CameraListener implements ImageCapture.OnImageSavedListener {

    @Override
    public void onImageSaved(@NonNull File file) {
        String msg = "Photo capture succeeded: " + file.getAbsolutePath();
        Log.d("CameraXApp", msg);
    }

    @Override
    public void onError(@NonNull ImageCapture.ImageCaptureError imageCaptureError, @NonNull String message, @Nullable Throwable cause) {
        String msg = "Photo capture failed: " + message;
        Log.e("CameraXApp", msg, cause);
    }
}
