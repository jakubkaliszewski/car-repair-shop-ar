package pl.kaliszewski.carworkshop.mobileapp;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import android.widget.ImageView;
import androidx.camera.core.ImageAnalysis;
import androidx.camera.core.ImageProxy;
import org.opencv.core.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.opencv.core.CvType.CV_8UC1;
import static org.opencv.core.CvType.CV_8UC3;
import static org.opencv.imgproc.Imgproc.*;

public class PlateDetectionAnalyzer implements ImageAnalysis.Analyzer {
    private ImageView imageView;

    public PlateDetectionAnalyzer(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    public void analyze(ImageProxy image, int rotationDegrees) {
        Mat matImage = ImageUtils.convertImageToMat(image);
        ImageUtils.rotateImage(matImage, rotationDegrees);

        Mat processImage = new Mat(matImage.size(), matImage.type());
        cvtColor(matImage, processImage, COLOR_BGR2GRAY);
        Mat blur = new Mat();
        bilateralFilter(processImage, blur, 11, 90, 90);
        Mat edges = new Mat();
        Canny(blur, edges, 30, 200);
        List<MatOfPoint> pointList = new ArrayList<>();
        Mat hierarchy = new Mat();
        findContours(edges, pointList, hierarchy, RETR_LIST, CHAIN_APPROX_TC89_L1);
        pointList.sort((o1, o2) -> contourArea(o1) >= contourArea(o2) ? 1 : -1);
        pointList = pointList.subList(0, pointList.size()/2);
        drawContours(matImage, pointList, -1, new Scalar(255, 0, 255), 4);//, 1, hierarchy);
        Bitmap bitmap = ImageUtils.createBitmapFromMat(matImage);
        //handler umożliwia wykonanie w innym wątku -> tu w głównym
        Handler handlerToUI = new Handler(Looper.getMainLooper());
        handlerToUI.post(() -> imageView.setImageBitmap(bitmap));
    }
    /*
    private void s(){
        try {
            MaskRcnnPlate model = MaskRcnnPlate.newInstance(context);

            // Creates inputs for reference.
            TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 256, 256, 3}, DataType.FLOAT32);
            inputFeature0.loadBuffer(byteBuffer);

            // Runs model inference and gets result.
            MaskRcnnPlate.Outputs outputs = model.process(inputFeature0);
            TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();
            TensorBuffer outputFeature1 = outputs.getOutputFeature1AsTensorBuffer();

            // Releases model resources if no longer used.
            model.close();
        } catch (IOException e) {
            // TODO Handle the exception
        }
    }
    */

}
