package pl.kaliszewski.carworkshop.mobileapp;

import android.graphics.Bitmap;
import android.media.Image;
import androidx.camera.core.ImageProxy;
import com.quickbirdstudios.yuv2mat.Yuv;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;

/**
 * Narzędzia do manipulacji na obrazie wykorzystywanych w procesie jego przetwarzania.
 */
public class ImageUtils {
    /**
     * Uzyskuje z ImageProxy macierz Mat zgodną z OpenCV.
     * @param imageProxy ImageProxy odczytanej klatki z kamery.
     * @return Zgodna z OpenCV macierz Mat w reprezentacji barw rgb.
     */
    public static Mat convertImageToMat(ImageProxy imageProxy){
        Image image = imageProxy.getImage();
        new Mat()
        Mat mat = Yuv.rgb(image);
        return mat;
    }

    /**
     * Obraca macierz obrazu względem informacji pozyskanej z sensora kamery.
     * @param image Obracana macierz obrazu
     * @param rotationDegrees Aktualne obrócenie obrazu
     */
    public static void rotateImage(Mat image, int rotationDegrees){
        switch (rotationDegrees){
            case 90:{
                org.opencv.core.Core.rotate(image, image, Core.ROTATE_90_CLOCKWISE);
                break;
            }
            case 180:{
                org.opencv.core.Core.rotate(image, image, Core.ROTATE_180);
                break;
            }
            case 270:{
                org.opencv.core.Core.rotate(image, image, Core.ROTATE_90_COUNTERCLOCKWISE);
                break;
            }
        }
    }

    /**
     * Utworzenie {@link Bitmap}y z macierzy {@link Mat} OpenCV
     * @param image Macierz {@link Mat} OpenCV
     * @return Bitmapa zawierająca obraz z macierzy.
     */
    public static Bitmap createBitmapFromMat(Mat image){
        Bitmap bitmap = Bitmap.createBitmap(image.cols(), image.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(image, bitmap);
        return bitmap;
    }
/*
    public static Bitmap cropBitmap(Bitmap bitmap) {
        float bitmapRatio = (float) bitmap.getHeight() / (float) bitmap.getWidth();
        float modelInputRatio = 1.f; //256 x 256
        Bitmap croppedBitmap = bitmap;

// Acceptable difference between the modelInputRatio and bitmapRatio to skip cropping.
        double maxDifference = 1e-5;

// Checks if the bitmap has similar aspect ratio as the required model input.
        when {
            abs(modelInputRatio - bitmapRatio) < maxDifference -> return croppedBitmap
            modelInputRatio < bitmapRatio -> {
                // New image is taller so we are height constrained.
                val cropHeight = bitmap.height - (bitmap.width.toFloat() / modelInputRatio)
                croppedBitmap = Bitmap.createBitmap(
                        bitmap,
                        0,
                        (cropHeight / 2).toInt(),
                        bitmap.width,
                        (bitmap.height - cropHeight).toInt()
                )
            }
  else -> {
                val cropWidth = bitmap.width - (bitmap.height.toFloat() * modelInputRatio)
                croppedBitmap = Bitmap.createBitmap(
                        bitmap,
                        (cropWidth / 2).toInt(),
                        0,
                        (bitmap.width - cropWidth).toInt(),
                        bitmap.height
                )
            }
        }
        return croppedBitmap;
    }*/
}
