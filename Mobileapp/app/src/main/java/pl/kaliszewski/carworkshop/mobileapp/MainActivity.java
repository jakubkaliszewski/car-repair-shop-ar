package pl.kaliszewski.carworkshop.mobileapp;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.Size;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.camera.core.*;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import org.opencv.android.Utils;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.tensorflow.lite.DataType;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.support.tensorbuffer.TensorBuffer;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import pl.kaliszewski.carworkshop.mobileapp.ml.MaskRcnnPlate;

import static org.opencv.core.CvType.CV_32F;
import static org.opencv.core.CvType.CV_32FC3;
import static org.opencv.core.CvType.CV_8S;
import static org.opencv.core.CvType.CV_8U;
import static org.opencv.core.CvType.CV_8UC3;

public class MainActivity extends AppCompatActivity {

    private final int REQUEST_CODE_PERMISSIONS = 10;
    private final String[] REQUIRED_PERMISSIONS = {Manifest.permission.CAMERA};
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //załadowanie opencv
        System.loadLibrary("opencv_java4");
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        imageView = findViewById(R.id.view_finder);

        if (allPermissionsGranted()) {
            imageView.post(this::startCamera);
        } else {
            ActivityCompat.requestPermissions(this, REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS);
        }
    }

    private void startCamera() {
        /*Analiza obrazu *///todo tak lepiej
        ImageAnalysisConfig imageAnalysisConfig = new ImageAnalysisConfig.Builder()
                .setTargetResolution(new Size(1980, 1080))
                .setImageReaderMode(ImageAnalysis.ImageReaderMode.ACQUIRE_LATEST_IMAGE)
                .build();
        ImageAnalysis imageAnalysis = new ImageAnalysis(imageAnalysisConfig);
        imageAnalysis.setAnalyzer(Executors.newCachedThreadPool(), new ImageAnalyzer(this));

        //imageAnalysis.setAnalyzer(Executors.newCachedThreadPool(), (image, rotationDegrees) -> {

        //});
        CameraX.bindToLifecycle(this, imageAnalysis);
    }

    /**
     * Process result from permission request dialog box, has the request
     * been granted? If yes, start Camera. Otherwise display a toast
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == REQUEST_CODE_PERMISSIONS) {
            if (allPermissionsGranted()) {
                imageView.post(this::startCamera);
            } else {
                Toast.makeText(this,
                        "Permissions not granted by the user.",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    /**
     * Check if all permission specified in the manifest have been granted
     */
    private boolean allPermissionsGranted() {
        for (String permission : REQUIRED_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(this, permission) == PackageManager.PERMISSION_DENIED)
                return false;
        }

        return true;
    }

    private class ImageAnalyzer implements ImageAnalysis.Analyzer {

        private MaskRcnnPlate model;

        public ImageAnalyzer(Context ctx) {
            try {
                model = MaskRcnnPlate.newInstance(ctx);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void analyze(ImageProxy image, int rotationDegrees) {

            /*Image prepare*/
            Mat matImage = ImageUtils.convertImageToMat(image);
            ImageUtils.rotateImage(matImage, rotationDegrees);
            //org.opencv.core.Size size = new org.opencv.core.Size(256, 256);
            //Imgproc.resize(matImage, matImage, size);

            Bitmap bitmap = ImageUtils.createBitmapFromMat(matImage);

            bitmap = Bitmap.createScaledBitmap(bitmap, 1024, 1024, true);
            ByteBuffer input = ByteBuffer.allocateDirect(1024 * 1024 * 3 * 4).order(ByteOrder.nativeOrder());
            for (int y = 0; y < 256; y++) {
                for (int x = 0; x < 256; x++) {
                    int px = bitmap.getPixel(x, y);

                    // Get channel values from the pixel value.
                    int r = Color.red(px);
                    int g = Color.green(px);
                    int b = Color.blue(px);

                    // Normalize channel values to [-1.0, 1.0]. This requirement depends
                    // on the model. For example, some models might require values to be
                    // normalized to the range [0.0, 1.0] instead.
                    float rf = (r - 127) / 255.0f;
                    float gf = (g - 127) / 255.0f;
                    float bf = (b - 127) / 255.0f;

                    input.putFloat(rf);
                    input.putFloat(gf);
                    input.putFloat(bf);
                }
            }


            try {
                //wykonać nowy model bez skalowania wejścia - zobacz python
                // Creates inputs for reference.
                TensorBuffer inputFeature0 = TensorBuffer.createFixedSize(new int[]{1, 1024, 1024, 3}, DataType.FLOAT32);
                inputFeature0.loadBuffer(input);

                // Runs model inference and gets result.
                MaskRcnnPlate.Outputs outputs = model.process(inputFeature0);
                TensorBuffer outputFeature0 = outputs.getOutputFeature0AsTensorBuffer();
                TensorBuffer outputFeature1 = outputs.getOutputFeature1AsTensorBuffer();

                // Releases model resources if no longer used.
                model.close();
            } catch (Exception e) {
                Log.d("MODEL", e.getMessage());
            }
        }
    }
}
